# YANG for IEEE Std 1588

Published YANG modules for IEEE Std 1588 are located in the repository that is designated for standardized YANG modules, in:

https://github.com/YangModels/yang/tree/main/standard/ieee/published/1588
